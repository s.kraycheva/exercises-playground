const axios = require('axios');

// get exchange rate
const getExchange = async (fromCurrency, toCurrency) => {
    const response = await axios.get('http://data.fixer.io/api/latest?access_key=f68b13604ac8e570a00f7d8fe7f25e1b&format=1')
    
    
        const rate = response.data.rates;
        const euro = 1 / rate[fromCurrency];
    const exchangeRate = euro * rate[toCurrency]

    if (isNaN(exchangeRate)) {
        throw new Error(`Unable to get currency ${fromCurrency} to ${toCurrency}`)
    }
    return exchangeRate;    
}

// get countries
const getCountries = async (toCurrency) => {
    try {
        const response = await axios.get(`https://restcountries.com/v2/currency/${toCurrency}`)
    return response.data.map(country=>country.name)
    } catch (error) {
        throw new Error(`Unable to get countries that use ${toCurrency}`)
    }
    
}
// convert function

const convertCurrency = async(fromCurrency, toCurrency, amount) => {
    const countries = await getCountries(toCurrency);
    const exchangeRate = await getExchange(fromCurrency,toCurrency);


    const convertedAmount = (amount * exchangeRate).toFixed(2);

    return `${amount} ${fromCurrency} is worth ${convertedAmount} ${toCurrency}. You can spend this in the following countries: ${countries}`
}

convertCurrency('USD', 'CAD', 30).then((message) => {
    console.log(message)
}).catch((error)=> {
    console.log(error.message);
})